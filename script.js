var searchbar = document.getElementById('searchbar');
var livesearch = document.getElementById('livesearch');

searchbar.addEventListener('input', search);
searchbar.focus();

function search() {
  var searchValue = searchbar.value;
  if (searchValue.length < 1) {
    livesearch.innerHTML = '';
    return;
  }
  // Ajax
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
  } else {  // code for IE6, IE5
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      // Where to put our result
      livesearch.innerHTML = this.responseText;
    }
  }
  // xmlhttp.open(method, url, asynchronous)
  xmlhttp.open("GET", "livesearch.php?q=" + searchValue, true);
  xmlhttp.send();
}

// ===================================

searchbar.addEventListener('keydown', function(event) {
  if (event.keyCode === 38) { // Up
    selectResult(-1); // Index - 1
  } else if (event.keyCode === 40) { // Down
    selectResult(1); // Index + 1
  }
});

var currentResult = -1;

// Select result with up and down
function selectResult(indexDiff) {
  var results = document.getElementsByClassName('result');
  if (currentResult >= 0 && currentResult < results.length) {
    results[currentResult].classList.remove('selected');
  }
  if (currentResult + indexDiff < 0) {
    currentResult = results.length - 1;
  } else if (currentResult + indexDiff >= results.length) {
    currentResult = 0;
  } else {
    currentResult += indexDiff;
  }
  results[currentResult].classList.add('selected');
  searchbar.value = results[currentResult].innerHTML;
}

// clickResult on click for each result
livesearch.addEventListener('mouseenter', function() {
  var results = document.getElementsByClassName('result');
  for (var i = 0; i < results.length; i++) {
    results[i].addEventListener('click', clickResult);
  }
});

// Select result on click
function clickResult() {
  var results = document.getElementsByClassName('result');
  for (var i = 0; i < results.length; i++) {
    results[i].classList.remove('selected');
  }
  searchbar.value = this.innerHTML;
  livesearch.innerHTML = '';
  searchbar.focus();
}
