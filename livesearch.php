<?php

// put the search value in a variable $q
$q = $_GET['q'];

// database infomations
$servername = 'localhost';
$username = 'root';
$password = '';
$db = 'divers';
$charset = 'utf8';

$dsn = "mysql:host=$servername;dbname=$db;charset=$charset";

// PDO
try {
  $pdo = new PDO($dsn, $username, $password);
} catch (\PDOException $e) {
  throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$sql = "SELECT name FROM pays WHERE name LIKE '%$q%'";

$query = $pdo->query($sql);

// Echo the result of the search
// if the query gives no result
if ($query->rowCount() === 0) {
  echo '<p class="result">Pas de résultat</p>';
} else { // if these is at least one result
  while ($row = $query->fetch()) {
    echo '<p class="result">';
    echo $row['name'];
    echo '</p>';
  }
}

?>
