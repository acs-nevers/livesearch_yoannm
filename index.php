<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Livesearch | Ajax PHP</title>
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700" rel="stylesheet">
</head>

<body>
  <header>
    <h1>Livesearch | Ajax PHP</h1>
  </header>

  <main>
    <h2>Recherche de pays</h2>
    <form>
      <input id="searchbar" type="search" placeholder="Recherche">
      <div id="livesearch"></div>
    </form>
  </main>

  <script type="text/javascript" src="script.js"></script>
</body>

</html>
